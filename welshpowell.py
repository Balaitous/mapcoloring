# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoringDialog
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from algorithm import Algorithm


class WelshPowell(Algorithm):

    name = 'Welsh - Powell'

    def run(self):
        vertex = range(len(self.m))
        vertex.sort(key=lambda i: len(self.m[i]), reverse=True)

        # Initialisation du tableau de couleur, indexé par le no de sommet
        vertexColor = [-1 for i in range(len(vertex))]

        # Affectation des couleurs par algorithme de Welsh-Powell
        currentColor = 0
        done = 0

        # Boucle sur la couleur
        while len(vertex) > 0:
            # Boucle sur les sommets
            i = 0
            while i < len(vertex):
                # Test si current color est possible
                if currentColor in [vertexColor[j] for j in self.m[vertex[i]]]:
                    i += 1
                else:
                    vertexColor[vertex[i]] = currentColor
                    del vertex[i]
                    done += 1
                    self.notify_progress(done*100/len(self.m))
            currentColor += 1
        return vertexColor
