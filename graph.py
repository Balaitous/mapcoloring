# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoringDialog
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsRectangle
from qgis.core import QgsGeometry

from monitor import Monitor


class PolygonsToGraph(Monitor):

    def __init__(self, layer, progress_cb=None):
        """
        @param layer QgsVectorLayer Layer to process
        @param progress_cb Callback to inform of algorithme progress
        """
        super(PolygonsToGraph, self).__init__(progress_cb)
        # Load polygons and bbox
        self.polygons = [QgsGeometry(feature.geometry())
                         for feature in layer.getFeatures()]
        self.bbox = [geom.boundingBox() for geom in self.polygons]

        # Initialisation des sommets adjacents
        nbFeat = layer.featureCount()
        self.m = [set() for i in range(nbFeat)]
        # Calcul récursif
        self.computeMatrixRec(range(nbFeat), 0.0, 100.0)

    def adjacency_matrix_list(self):
        return self.m

    def computeMatrixAll(self, vertice):
        """
        Calcul la matrice de connection en testant toutes les combinaisons
        """
        for i in range(len(vertice) - 1):
            for j in range(i + 1, len(vertice)):
                polyi = self.polygons[vertice[i]]
                polyj = self.polygons[vertice[j]]
                if polyi.touches(polyj):
                    inter = polyi.intersection(polyj)
                    if inter is not None and inter.type() in [1, 2]:
                        self.m[vertice[i]].add(vertice[j])
                        self.m[vertice[j]].add(vertice[i])

    def computeMatrixRec(self, vertice, progressFrom, progressTo):
        if len(vertice) <= 15:
            self.computeMatrixAll(vertice)
        else:
            # Calcul récursif après partage en deux
            # Calcul BBox
            bbox0 = QgsRectangle(self.bbox[vertice[0]])
            for i in range(1, len(vertice)):
                bbox0.combineExtentWith(self.bbox[vertice[i]])
            # Partage en deux de la bbox
            if bbox0.width() > bbox0.height():
                # Partage vertical
                medium = (bbox0.xMinimum() + bbox0.xMaximum()) / 2
                bbox1 = QgsRectangle(bbox0)
                bbox1.setXMaximum(medium)
                bbox2 = QgsRectangle(bbox0)
                bbox2.setXMinimum(medium)
            else:
                # Partage horizontal
                medium = (bbox0.yMinimum() + bbox0.yMaximum()) / 2
                bbox1 = QgsRectangle(bbox0)
                bbox1.setYMaximum(medium)
                bbox2 = QgsRectangle(bbox0)
                bbox2.setYMinimum(medium)
            # Construction des deux sous groupes
            vertice1 = [s for s in vertice if self.bbox[s].intersects(bbox1)]
            vertice2 = [s for s in vertice if self.bbox[s].intersects(bbox2)]
            # Appel récursif sur les deux sous groupes
            if len(vertice) == len(vertice1) or len(vertice) == len(vertice2):
                # Échec du partage en deux, recherche sur l'ensemble des
                # combinaisons possibles
                self.computeMatrixAll(vertice)
            else:
                progressMedium = (progressFrom + progressTo) / 2
                self.computeMatrixRec(vertice1, progressFrom, progressMedium)
                self.computeMatrixRec(vertice2, progressMedium, progressTo)
        self.notify_progress(progressTo)
