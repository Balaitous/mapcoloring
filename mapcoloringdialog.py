# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoringDialog
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4.QtCore import QFileInfo
from PyQt4.QtCore import QObject
from PyQt4.QtCore import QSettings
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import QThread
from PyQt4.QtGui import QDialog
from PyQt4.QtGui import QFileDialog
from PyQt4.QtGui import QMessageBox

from qgis.core import QGis
from qgis.core import QgsMapLayer
from qgis.core import QgsMapLayerRegistry
from qgis.gui import QgsEncodingFileDialog

from ui_mapcoloring import Ui_MapColoring

from dsatur import DSATUR
from welshpowell import WelshPowell
from five import FiveColor
from worker import Worker


def safe(fct):
    """
    Abort dialog if exception occur in main thread.
    """
    def decorated(self, *args, **kwargs):
        try:
            fct(self, *args, **kwargs)
        except:
            QDialog.reject(self)
            raise
    return decorated


class NoAvailableLayer(Exception):
    """
    Raise if there is no polygon layers.
    """
    pass


class MapColoringDialog(QDialog, Ui_MapColoring):
    """
    Dialog for MapColoring plugin.
    """

    ALGORITHMS = [WelshPowell, DSATUR, FiveColor]

    def __init__(self, iface):
        QDialog.__init__(self)
        self.iface = iface
        self.setupUi(self)
        QObject.connect(self.saveAsButton, SIGNAL("clicked()"), self.outFile)

        # Fill input layer ComboBox
        layers = []
        for layer in QgsMapLayerRegistry.instance().mapLayers().values():
            if layer.type() == QgsMapLayer.VectorLayer and \
                    layer.geometryType() == QGis.Polygon:
                layers.append((layer.name(), layer.id()))
        if len(layers) == 0:
            raise NoAvailableLayer()
        layers.sort(key=lambda x: x[0].lower())
        for name, id in layers:
            self.inputLayerCombo.addItem(name, id)

        # Pre-select active layer
        active_layer = iface.activeLayer()
        if active_layer.type() == QgsMapLayer.VectorLayer and \
               active_layer.geometryType() == QGis.Polygon:
            index = self.inputLayerCombo.findData(active_layer.id())
            self.inputLayerCombo.setCurrentIndex(index)

        # Fill algorithm ComboBox
        for algorithm in self.ALGORITHMS:
            self.algorithmeCombo.addItem(algorithm.name)
        self.algorithmeCombo.setCurrentIndex(0)

        self.working = False
        self.encoding = 'utf-8'

    def outFile(self):
        self.outputLayerEdit.clear()
        self.shapefileName, self.encoding = self.saveDialog(self)
        if self.shapefileName is None or self.encoding is None:
            return
        self.outputLayerEdit.setText(self.shapefileName)

    def saveDialog(self, parent):
        """
        Open a file dialog to choose output filename
        """
        settings = QSettings()
        dirName = settings.value("/UI/lastShapefileDir")
        filtering = "Shapefiles (*.shp)"
        coding = settings.value("/UI/encoding")
        fileDialog = QgsEncodingFileDialog(
            parent, "Save output shapefile", dirName, filtering, coding)
        fileDialog.setDefaultSuffix("shp")
        fileDialog.setFileMode(QFileDialog.AnyFile)
        fileDialog.setAcceptMode(QFileDialog.AcceptSave)
        fileDialog.setConfirmOverwrite(True)
        if not fileDialog.exec_() == QDialog.Accepted:
            return None, None
        files = fileDialog.selectedFiles()
        if files:
            settings.setValue("/UI/lastShapefileDir",
                              QFileInfo(files[0]).absolutePath())
        return (files[0], fileDialog.encoding())

    @safe
    def accept(self):
        """
        Callback to OK button
        """
        if self.working:
            return
        if self.inputLayerCombo.currentText() == "":
            QMessageBox.warning(
                self, "Error", self.tr("Please specify input vector layer."))
            return
        if self.btn_shapefile.isChecked():
            if self.outputLayerEdit.text() == "":
                QMessageBox.warning(
                    self, "Error", self.tr("Please specify output shapefile."))
                return
            shapefile_name = self.outputLayerEdit.text()
        elif self.btn_memory.isChecked():
            shapefile_name = None
        else:
            QMessageBox.warning(
                self, "Error", self.tr("Please specify output method."))
            return
        if self.encoding == "":
            self.encoding = "utf-8"
        layer_id = self.inputLayerCombo.itemData(
            self.inputLayerCombo.currentIndex())
        layer = QgsMapLayerRegistry.instance().mapLayer(layer_id)
        algo_id = self.algorithmeCombo.currentIndex()
        worker = Worker(layer, shapefile_name,
                        self.encoding,
                        self.keepFieldsCheckBox.isChecked(),
                        self.ALGORITHMS[algo_id],
                        self.colorField.text())
        # start the worker in a new thread
        self.working = True
        self.main_widget.setDisabled(True)
        thread = QThread(self)
        worker.moveToThread(thread)
        worker.finished.connect(self.finished)
        worker.progress.connect(self.progressBar.setValue)
        worker.notify.connect(self.stateLabel.setText)
        self.worker = worker
        self.thread = thread
        thread.started.connect(self.worker.run)
        thread.start()

    def reject(self):
        if self.working:
            self.worker.kill()
        else:
            super(MapColoringDialog, self).reject()

    @safe
    def finished(self, status, layer):
        """
        slot called when worker finished
        @param status
            0: Work done
            1: Abort
            2: Exception
        @param layer Resulting layer to be add
        """
        if status == 0:
            # Add layer to map
            QgsMapLayerRegistry.instance().addMapLayer(layer)
        elif status == 2:
            QMessageBox.critical(self, "Error", self.worker.traceback_str)
        self.worker.deleteLater()
        self.thread.quit()
        self.thread.wait()
        self.thread.deleteLater()
        if status == 0:
            super(MapColoringDialog, self).accept()
        else:
            super(MapColoringDialog, self).reject()
