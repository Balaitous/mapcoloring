# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoringDialog
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import traceback

from PyQt4.QtCore import QObject
from PyQt4.QtCore import QVariant
from PyQt4.QtCore import pyqtSignal

from qgis.core import QgsField
from qgis.core import QgsFields
from qgis.core import QgsMapLayerRegistry
from qgis.core import QgsVectorFileWriter
from qgis.core import QgsVectorLayer

from graph import PolygonsToGraph
from monitor import Killed


class Worker(QObject):
    """
    Class doing the job.
    """

    def __init__(self, layer, shapefile_name, encoding, keep_attributes,
                 algorithm, color_fieldname):
        """
        @param layer (QgsVectorLayer) Input data layer
        @param shapefile_name Output shapefile
        @param encoding Encodage du shapefile
        @param keep_attributes Keep layer attributes in output shapefile
        @param algorithm Algorithme class
        @param color_fieldname Name of the field storing color index
        """
        super(Worker, self).__init__()
        self.layer = layer
        self.shapefile_name = shapefile_name
        self.encoding = encoding
        self.keep_attributes = keep_attributes
        self.algorithm = algorithm
        self.color_fieldname = color_fieldname
        self.killed = False

    def run(self):
        try:
            self.real_run()
            self.finished.emit(0, self.output_layer)
        except Killed:
            self.finished.emit(1, None)
        except Exception:
            self.traceback_str = traceback.format_exc()
            self.finished.emit(2, None)

    def real_run(self):
        nbFeat = self.layer.featureCount()

        # Matrice des connections
        self.notify.emit(self.tr(u"Calculating connexion graph ..."))
        m = PolygonsToGraph(
            self.layer, self.set_progress).adjacency_matrix_list()

        # Classement des sommets par nombre de connections, et liste des sommets adjacents
        self.notify.emit(self.tr(u"Map coloring ..."))
        self.set_progress(0)
        vertexColor = self.algorithm(m, self.set_progress).vertexColor()

        # Enregistrement dans le fichier final
        if self.shapefile_name is None:
            self.notify.emit(self.tr("Write result to memory layer ..."))
        else:
            self.notify.emit(self.tr("Write result to shapefile ..."))
        self.set_progress(0)
        # Liste des champs de données
        fields = QgsFields()
        if self.keep_attributes:
            for field in self.layer.dataProvider().fields():
                fields.append(QgsField(field.name(), field.type()))
        fields.append(QgsField(self.color_fieldname, QVariant.Int))

        # Création du fichier
        if self.shapefile_name is None:
            writer = QgsVectorLayer("Polygon", self.mk_layer_name(), "memory")
            writer.startEditing()
            for field in fields:
                writer.addAttribute(field)
        else:
            pr = self.layer.dataProvider()
            writer = QgsVectorFileWriter(
                self.shapefile_name, self.encoding, fields,
                pr.geometryType(), pr.crs())

        # Insertion des polygones
        done = 0
        for i, feature in enumerate(self.layer.getFeatures()):
            if self.keep_attributes:
                attributes = feature.attributes()
            else:
                attributes = []
            attributes.append(vertexColor[i])
            feature.setAttributes(attributes)
            writer.addFeature(feature)
            done += 1
            self.set_progress(done * 100 / nbFeat)

        # Fermeture du fichier
        if self.shapefile_name is None:
            writer.commitChanges()
            self.output_layer = writer
        else:
            del writer
            self.output_layer = QgsVectorLayer(
                self.shapefile_name,
                os.path.basename(str(self.shapefile_name)), "ogr")

    def mk_layer_name(self):
        prefix = "%s_colormap" % self.layer.name()
        names = [layer.name()
                 for layer in QgsMapLayerRegistry.instance().mapLayers().values()
                 if layer.name().startswith(prefix)]
        if prefix not in names:
            return prefix
        index = 1
        while True:
            name = '%s_%i' % (prefix, index)
            if name not in names:
                return name
            index += 1

    def set_progress(self, value):
        if self.killed:
            raise Killed
        self.progress.emit(value)

    def kill(self):
        self.killed = True

    finished = pyqtSignal(int, object)
    progress = pyqtSignal(float)
    notify = pyqtSignal(unicode)
