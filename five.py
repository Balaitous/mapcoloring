# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoring
                                 A QGIS plugin
 Coloring a map with minimal number of color
                              -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from collections import deque

from algorithm import Algorithm
from util import tr


class Vertex(object):
    """
    Vertex in a graph.
    """

    def __init__(self, i, neighbours, component=[]):
        """
        Constructor
        @param i Index of vertex
        @param neighbours set() of adjacent vertices
        @param component If this vertex result of joining vertices, store
            vertices which are joined.
        @param color Color of vertex
        """
        self.i          = i
        self.component  = component
        self.neighbours = neighbours
        self.color      = None

    def colorize(self, c):
        """
        Apply a color to a vertex.
        Recurse to all component.
        @param c Color number
        """
        self.color = c
        for vertex in self.component:
            vertex.colorize(c)

    def __len__(self):
        """
        len(vertex) is the degree.
        """
        return len(self.neighbours)


class Graph(object):
    """
    More elaborate class moralizing a graph.
    """

    def __init__(self, m):
        """
        Constructor.
        @param m List of adjency for each vertice
        """
        self._data = {i: Vertex(i, neighbours)
                      for i, neighbours in enumerate(m)}
        self._last = len(self._data)

    def remove(self, i):
        """
        Remove vertex.
        @param i Index of vertex
        @return Vertex removed
        """
        vertice = self._data[i]
        del self._data[i]
        for j in vertice.neighbours:
            self._data[j].neighbours.remove(i)
        return vertice

    def insert(self, vertex):
        """
        Insert a vertex in this graph.
        """
        i = vertex.i
        self._data[i] = vertex
        for j in vertex.neighbours:
            self._data[j].neighbours.add(i)

    def join(self, u, v):
        """
        When reducing a graph, join 2 vertex in one, updating adjacency.
        @param u, v Index vertices to be joined
        @return Index of resulting vertex
        """
        data = self._data
        i0 = self._last
        vertex = Vertex(i0, data[u].neighbours | data[v].neighbours,
                        [data[u], data[v]])
        self.remove(u)
        self.remove(v)
        for i in vertex.neighbours:
            data[i].neighbours.add(i0)
        self._data[i0] = vertex
        self._last += 1
        return i0

    def split(self, i):
        """
        When a vertex result of joining 2 vertices, split this vertex into
        originating vertices.
        @param i Index of vertex
        """
        for vertex in self._data[i].component:
            self.insert(vertex)
        self.remove(i)

    def vertex(self, i):
        """
        Return vertex of index i.
        """
        return self._data[i]

    def vertices(self):
        """
        Return vertices dictionary."
        """
        return self._data

    def __len__(self):
        """
        len(Graph) is the number of vertices.
        """
        return len(self._data)


class FiveColor(Algorithm):
    """
    This algorithm color a planar graph with 5 colors in linear time.
    """

    name = tr(u"Five Color - Linear time")

    def run(self):
        # Init
        graph = Graph(self.m)
        collapse = deque()

        # Reduce graph
        while len(graph) > 4:
            done = float(len(graph)) / float(len(self.m))
            self.notify_progress((1 - done) * 50)
            for i, vertex in graph.vertices().items():
                if len(vertex) <= 5:
                    break
            if len(vertex) > 5:
                raise RuntimeError(tr("No vertex of degree at most 5."))
            graph.remove(i)
            joined = None
            if len(vertex) == 5:
                for i in vertex.neighbours:
                    for j in vertex.neighbours:
                        if i != j and i not in graph.vertex(j).neighbours:
                            joined = graph.join(i, j)
                            break
                    if joined is not None:
                        break
                if joined is None:
                    raise RuntimeError(tr("Can't find two non adjacent vertex."))
            collapse.append((vertex, joined))

        # Trivial color
        for c, vertex in enumerate(graph.vertices().values()):
            vertex.colorize(c)

        # Recontruct graph
        while len(collapse) > 0:
            done = float(len(graph)) / float(len(self.m))
            self.notify_progress(50 + (1 - done) * 50)
            vertex, joined = collapse.pop()
            if joined is not None:
                graph.split(joined)
            used = set([graph.vertex(i).color for i in vertex.neighbours])
            for c in range(5):
                if c not in used:
                    vertex.colorize(c)
                    break
            graph.insert(vertex)

        # Final result
        colorindex = [graph.vertex(i).color for i in range(len(graph))]
        return colorindex
