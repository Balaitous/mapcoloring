# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mapcoloring.ui'
#
# Created: Sun Apr  5 14:59:52 2015
#      by: PyQt4 UI code generator 4.11.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MapColoring(object):
    def setupUi(self, MapColoring):
        MapColoring.setObjectName(_fromUtf8("MapColoring"))
        MapColoring.resize(422, 392)
        self.verticalLayout = QtGui.QVBoxLayout(MapColoring)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.main_widget = QtGui.QFrame(MapColoring)
        self.main_widget.setFrameShape(QtGui.QFrame.NoFrame)
        self.main_widget.setObjectName(_fromUtf8("main_widget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.main_widget)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.grp_input = QtGui.QGroupBox(self.main_widget)
        self.grp_input.setObjectName(_fromUtf8("grp_input"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.grp_input)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.inputLayerCombo = QtGui.QComboBox(self.grp_input)
        self.inputLayerCombo.setObjectName(_fromUtf8("inputLayerCombo"))
        self.verticalLayout_3.addWidget(self.inputLayerCombo)
        self.verticalLayout_2.addWidget(self.grp_input)
        self.grp_output = QtGui.QGroupBox(self.main_widget)
        self.grp_output.setObjectName(_fromUtf8("grp_output"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.grp_output)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.btn_shapefile = QtGui.QRadioButton(self.grp_output)
        self.btn_shapefile.setObjectName(_fromUtf8("btn_shapefile"))
        self.horizontalLayout_2.addWidget(self.btn_shapefile)
        self.outputLayerEdit = QtGui.QLineEdit(self.grp_output)
        self.outputLayerEdit.setObjectName(_fromUtf8("outputLayerEdit"))
        self.horizontalLayout_2.addWidget(self.outputLayerEdit)
        self.saveAsButton = QtGui.QPushButton(self.grp_output)
        self.saveAsButton.setObjectName(_fromUtf8("saveAsButton"))
        self.horizontalLayout_2.addWidget(self.saveAsButton)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.btn_memory = QtGui.QRadioButton(self.grp_output)
        self.btn_memory.setChecked(True)
        self.btn_memory.setObjectName(_fromUtf8("btn_memory"))
        self.verticalLayout_4.addWidget(self.btn_memory)
        self.verticalLayout_2.addWidget(self.grp_output)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.grp_params = QtGui.QGroupBox(self.main_widget)
        self.grp_params.setObjectName(_fromUtf8("grp_params"))
        self.formLayout = QtGui.QFormLayout(self.grp_params)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.lbl_attr = QtGui.QLabel(self.grp_params)
        self.lbl_attr.setObjectName(_fromUtf8("lbl_attr"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.lbl_attr)
        self.colorField = QtGui.QLineEdit(self.grp_params)
        self.colorField.setObjectName(_fromUtf8("colorField"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.colorField)
        self.lbl_algo = QtGui.QLabel(self.grp_params)
        self.lbl_algo.setObjectName(_fromUtf8("lbl_algo"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.lbl_algo)
        self.algorithmeCombo = QtGui.QComboBox(self.grp_params)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.algorithmeCombo.sizePolicy().hasHeightForWidth())
        self.algorithmeCombo.setSizePolicy(sizePolicy)
        self.algorithmeCombo.setObjectName(_fromUtf8("algorithmeCombo"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.algorithmeCombo)
        self.keepFieldsCheckBox = QtGui.QCheckBox(self.grp_params)
        self.keepFieldsCheckBox.setChecked(True)
        self.keepFieldsCheckBox.setObjectName(_fromUtf8("keepFieldsCheckBox"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.keepFieldsCheckBox)
        self.verticalLayout_2.addWidget(self.grp_params)
        self.verticalLayout.addWidget(self.main_widget)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.stateLabel = QtGui.QLabel(MapColoring)
        self.stateLabel.setText(_fromUtf8(""))
        self.stateLabel.setObjectName(_fromUtf8("stateLabel"))
        self.verticalLayout.addWidget(self.stateLabel)
        self.progressBar = QtGui.QProgressBar(MapColoring)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.verticalLayout.addWidget(self.progressBar)
        self.line = QtGui.QFrame(MapColoring)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.buttonBox = QtGui.QDialogButtonBox(MapColoring)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(MapColoring)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), MapColoring.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), MapColoring.reject)
        QtCore.QMetaObject.connectSlotsByName(MapColoring)

    def retranslateUi(self, MapColoring):
        MapColoring.setWindowTitle(_translate("MapColoring", "MapColoringPlugin", None))
        self.grp_input.setTitle(_translate("MapColoring", "Input vector layer of polygons", None))
        self.grp_output.setTitle(_translate("MapColoring", "Output layer", None))
        self.btn_shapefile.setText(_translate("MapColoring", "Shapefile", None))
        self.saveAsButton.setText(_translate("MapColoring", "...", None))
        self.btn_memory.setText(_translate("MapColoring", "Memory layer", None))
        self.grp_params.setTitle(_translate("MapColoring", "Parameters", None))
        self.lbl_attr.setText(_translate("MapColoring", "Color index attribute :", None))
        self.colorField.setText(_translate("MapColoring", "COLORID", None))
        self.lbl_algo.setText(_translate("MapColoring", "Algorithm :", None))
        self.keepFieldsCheckBox.setText(_translate("MapColoring", "Keep existing attributes", None))

