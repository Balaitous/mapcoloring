# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoring
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


def metadata():
    if not hasattr(metadata, '_metadata'):
        import ConfigParser
        config = ConfigParser.RawConfigParser()
        config.read('metadata.txt')
        setattr(metadata, '_metadata', config)
    return metadata._metadata


def version():
    """
    Return plugin version.
    """
    return metadata().get('general', 'version')


def classFactory(iface):
    # load MapColoring class from file MapColoring
    from mapcoloring import MapColoring
    return MapColoring(iface)
