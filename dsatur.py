# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoringDialog
                                 A QGIS plugin
 Coloring a map with minimal number of color
                             -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from algorithm import Algorithm


class DSATUR(Algorithm):

    name = 'DSATUR'

    def run(self):
        # Classement des sommets en fonction de leur nombre DSAT
        DSAT = []
        vertexDSAT = [len(adj) for adj in self.m]
        DSAT = [[] for i in range(max(vertexDSAT)+1)]
        for i in range(len(self.m)):
            # Ajout à la liste DSAT
            DSAT[vertexDSAT[i]].append(i)

        # Algorithme principal:
        vertexColor = [-1 for i in range(len(self.m))]
        done = 0
        # Boucle de coloration
        while len(DSAT) > 0:
            # Sommet suivant à colorer : nDSAT max, et si égalité degré max
            currentVertex = DSAT[len(DSAT)-1][0]
            # couleur à utiliser : la plus petite possible
            colorUsed = [vertexColor[i]
                         for i in self.m[currentVertex] if vertexColor[i] != -1]
            colorUsed.sort()
            currentColor = 0
            for i in colorUsed:
                if i > currentColor:
                    break
                currentColor = i+1
            # coloration du sommet
            vertexColor[currentVertex] = currentColor
            # Suppression du sommet currentVertex des liste DSAT
            del DSAT[len(DSAT)-1][0]

            # Mise à jour des nombres DSAT, Itération sur les sommets adjacents au sommet qui vient d'être coloré
            for i in self.m[currentVertex]:
                if vertexColor[i] != -1: # Le sommet est déjà coloré, rien à faire
                    continue
                # Calcul du nouveau DSAT
                newDSAT = len([1 for s in self.m[i] if vertexColor[s] != -1])
                # Suppression de i dans DSAT[oldDSAT]
                oldDSAT = vertexDSAT[i]
                DSAT[oldDSAT].remove(i)
                # Ajoute de i dans DSAT[newDSAT], trie par degré décroissant
                while newDSAT >= len(DSAT):
                    DSAT.append([])
                k = 0
                while k < len(DSAT[newDSAT]):
                    if len(self.m[DSAT[newDSAT][k]]) < len(self.m[i]):
                        break
                    k += 1
                DSAT[newDSAT].insert(k, i)
                # Mise à jour du nombre DSAT dans vertex
                vertexDSAT[i] = newDSAT
            # Nétoyage de DSAT
            while len(DSAT) > 0:
                if len(DSAT[len(DSAT)-1]) > 0:
                    break
                DSAT.pop()
            done += 1
            self.notify_progress(done*100/len(self.m))

        return vertexColor
