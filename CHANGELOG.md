# Change Log

## [0.2] 2015-04-07
### Changed
* Update to QGis 2.x (2.8 tested)
* Split code
* Allow user to cancel after start algorithm

## [0.1] 2012-10-01
First version
