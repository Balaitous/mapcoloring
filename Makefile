# /***************************************************************************
#  MapColoring
#                                  A QGIS plugin
#  Coloring a map with minimal number of color
#                              -------------------
#         begin                : 2012-09-24
#         copyright            : (C) 2012-2015 by Alain Delplanque
#         email                : alaindelplanque@laposte.net
#  ***************************************************************************/

# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/


UI_FILES = ui_mapcoloring.ui

SOURCES = \
	__init__.py \
	algorithm.py \
	dsatur.py \
	five.py \
	graph.py \
	mapcoloring.py \
	mapcoloringdialog.py \
	monitor.py \
	util.py \
	welshpowell.py \
	worker.py \
	$(UI_FILES:.ui=.py)

PLUGIN_NAME = MapColoring
PLUGIN_NAME_LOWER = mapcoloring

EXTRAS = \
	metadata.txt \
	README.md \
	CHANGELOG.md \
	icon.svg icon.png

QGISDIR=.qgis2

PLUGIN_VERSION = $(shell python -c "from __init__ import version; print(version())")
ARCHIVE = $(PLUGIN_NAME)-$(PLUGIN_VERSION).zip

LOCALES = fr
QM_FILES = $(addsuffix .qm, $(addprefix i18n/$(PLUGIN_NAME_LOWER)_, $(LOCALES)))
TS_FILES = $(addsuffix .ts, $(addprefix i18n/$(PLUGIN_NAME_LOWER)_, $(LOCALES)))

ui_%.py: ui_%.ui
	pyuic4 $< -o $@

ts_update: $(TS_FILES)

qm_update: $(QM_FILES)

%.ts: $(SOURCES)
	cd i18n && pylupdate4 $(addprefix ../, $(SOURCES)) -ts $(notdir $@)
	sed -i 's/<location filename="/<location filename="..\//g' $@

%.qm: %.ts
	cd i18n && lrelease-qt4 $(notdir $<)

deploy: $(SOURCES) $(EXTRAS) $(QM_FILES)
	@echo
	@echo "------------------------------------------"
	@echo "Deploying plugin to your .qgis2 directory."
	@echo "------------------------------------------"
	rm -rf $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)
	mkdir -p $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)
	cp -vf --parents $(SOURCES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)
	cp -vf --parents $(EXTRAS) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)
	cp -vf --parents $(QM_FILES) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)

devlink: $(SOURCES) $(EXTRAS) $(QM_FILES)
	@echo
	@echo "------------------------------------------"
	@echo "Deploying plugin to your .qgis2 directory."
	@echo "(developement mode)"
	@echo "------------------------------------------"
	rm -rf $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)
	ln -s $(shell pwd) $(HOME)/$(QGISDIR)/python/plugins/$(PLUGIN_NAME)

$(ARCHIVE): $(SOURCES) $(EXTRAS) $(QM_FILES)
	rm -rf $(PLUGIN_NAME_LOWER)
	mkdir $(PLUGIN_NAME_LOWER)
	cp -vf --parents $(SOURCES) $(EXTRAS) $(QM_FILES) $(PLUGIN_NAME_LOWER)
	zip -r $@ $(PLUGIN_NAME_LOWER)
	rm -rf $(PLUGIN_NAME_LOWER)

zip: $(ARCHIVE)

clean:
	rm -f *.pyc
	rm -f *~
