== MapColoring ==

This plugin performs a coloration of a set of polygons, ensuring that two contiguous polygons does not have the same color.

It take a vector layer of polygons as input, and write them to a layer.
A new attribute is created with index of a color.

== Algorithms ==

The algorithm does not guarantee a minimum number of colors, but try to minimize it.

Two algorithms are implemented:
* Welsh-Powell
* DSATUR
