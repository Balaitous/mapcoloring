# -*- coding: utf-8 -*-
"""
/***************************************************************************
 MapColoring
                                 A QGIS plugin
 Coloring a map with minimal number of color
                              -------------------
        begin                : 2012-09-24
        copyright            : (C) 2012-2015 by Alain Delplanque
        email                : alaindelplanque@laposte.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# Import the PyQt and QGIS libraries
from PyQt4.QtCore import QCoreApplication
from PyQt4.QtCore import QFileInfo
from PyQt4.QtCore import QObject
from PyQt4.QtCore import QSettings
from PyQt4.QtCore import QTranslator
from PyQt4.QtCore import SIGNAL
from PyQt4.QtCore import qVersion
from PyQt4.QtGui import QAction
from PyQt4.QtGui import QIcon
from PyQt4.QtGui import QMessageBox

from qgis.core import QgsApplication

from mapcoloringdialog import MapColoringDialog
from mapcoloringdialog import NoAvailableLayer
from util import tr


class MapColoring:
    """
    Plugin class for MapColoring.
    """

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = \
            QFileInfo(QgsApplication.qgisUserDbFilePath()).path() + \
            "/python/plugins/MapColoring"
        # initialize locale
        localePath = ""
        locale = QSettings().value("locale/userLocale")[0:2]
        if QFileInfo(self.plugin_dir).exists():
            localePath = self.plugin_dir + "/i18n/mapcoloring_" + locale + ".qm"
        if QFileInfo(localePath).exists():
            self.translator = QTranslator()
            self.translator.load(localePath)
            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/mapcoloring/icon.png"),
            u"Map Coloring", self.iface.mainWindow())
        # connect the action to the run method
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Map Coloring", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Map Coloring", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        try:
            dlg = MapColoringDialog(self.iface)
            dlg.exec_()
        except NoAvailableLayer:
            QMessageBox.warning(
                None, "Error",
                tr(u"No polygons layer available."))
