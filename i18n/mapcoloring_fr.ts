<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0" language="fr" sourcelanguage="en">
<context>
    <name>MapColoring</name>
    <message>
        <location filename="../mapcoloring.py" line="89"/>
        <source>No polygons layer available.</source>
        <translation>Aucune couche de polygones n&apos;est disponible.</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="124"/>
        <source>MapColoringPlugin</source>
        <translation>MapColoringPlugin</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="125"/>
        <source>Input vector layer of polygons</source>
        <translation>Couche vectorielle de polygones en entrée</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="126"/>
        <source>Output layer</source>
        <translation>Couche de sortie</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="127"/>
        <source>Shapefile</source>
        <translation>Shapefile</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="128"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="129"/>
        <source>Memory layer</source>
        <translation>Couche mémoire</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="130"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="131"/>
        <source>Color index attribute :</source>
        <translation>Attribut pour la couleur :</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="132"/>
        <source>COLORID</source>
        <translation>COLORID</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="133"/>
        <source>Algorithm :</source>
        <translation>Algorithme :</translation>
    </message>
    <message>
        <location filename="../ui_mapcoloring.py" line="134"/>
        <source>Keep existing attributes</source>
        <translation>Conserver les attributs de la couche d&apos;entrée</translation>
    </message>
</context>
<context>
    <name>MapColoringDialog</name>
    <message>
        <location filename="../mapcoloringdialog.py" line="141"/>
        <source>Please specify input vector layer.</source>
        <translation>Merci d&apos;indiquer une couche d&apos;entrée.</translation>
    </message>
    <message>
        <location filename="../mapcoloringdialog.py" line="146"/>
        <source>Please specify output shapefile.</source>
        <translation>Merci de choisir un fichier shapefile de sortie.</translation>
    </message>
    <message>
        <location filename="../mapcoloringdialog.py" line="153"/>
        <source>Please specify output method.</source>
        <translation>Merci d&apos;indiquer la nature de la couche de sortie.</translation>
    </message>
</context>
<context>
    <name>Worker</name>
    <message>
        <location filename="../worker.py" line="78"/>
        <source>Calculating connexion graph ...</source>
        <translation>Calcul de la matrice de connexion ...</translation>
    </message>
    <message>
        <location filename="../worker.py" line="83"/>
        <source>Map coloring ...</source>
        <translation>Coloration du graph ...</translation>
    </message>
    <message>
        <location filename="../worker.py" line="89"/>
        <source>Write result to memory layer ...</source>
        <translation>Ecriture du résultat dans une couche mémoire ...</translation>
    </message>
    <message>
        <location filename="../worker.py" line="91"/>
        <source>Write result to shapefile ...</source>
        <translation>Ecriture du résultat dans un fichier Shapefile ...</translation>
    </message>
</context>
</TS>
